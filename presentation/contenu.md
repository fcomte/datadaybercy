<!-- .slide: class="slide" -->
# Les 4 piliers de la transformation technologique...

1. Pilier ingénierie logicielle : la gestion de source et le déploiement continu
2. Pilier processus de traitement : la conteneurisation
3. Pilier stockage : entrepot S3
4. Pilier expérience utilisateur : place à la démo

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Pour soutenir 3 valeurs

- l'open-source
- la collaboration
- la statistique reproductible

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Dans un contexte

- 5300 agents dont :
    - 1000 développeurs statisticiens (réguliers ou occasionnels) (SAS , R , python ...)
    - 200 développeurs dans les services informatiques informaticiens (Java, Javascript ...)


---

<!-- .slide: class="slide" -->
# Pilier ingénierie logicielle

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Gestion de source

- Patrimoine de code  
- Gestion de version
- Visibilité, collaboration, partage

=> Git & Gitlab / Github / Bitbucket

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Intégration / déploiement continu

- Automatisation
- Fiabilisation  
- Traçabilité
- Reproductibilité

=> Gitlab-ci, travis-ci, Jenkins

---

<!-- .slide: class="slide" -->
# Pilier processus de traitement

----

<!-- .slide: data-background-image="logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Infrastructure mutualisée de traitemenent de données

Les problématiques usuelles :

- le partage de ressources entre les utilisateurs
- l'adhérence à la configuration système

----

<img src=".\images\container.png" width="50%"  height="50%" class="center">

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Les avantages

- Affectation fine et efficace de ressources
- Agnostique du langage
- Indépendant de la configuration système

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Illustration dans l'eco-système BigData

Concurrence forte :

- cloud computing
- acteur de la conteneurisation

https://www.lemondeinformatique.fr/actualites/lire-cloudera-revoit-a-la-baisse-ses-previsions-sur-l-exercice-2020-75584.html

---

<!-- .slide: class="slide" -->
# Pilier stockage

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Problématique 

- l'accès à des fichiers d'input ou d'output de traitements statisques

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Le partage réseau ? 

Mauvaises propriétés :
- configuration multipartite
- ignorance de l'application sur le caractère distant de la ressource
- fonctionnalité au delà du strict nécessaire

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## S3 (Simple Storage Service)

Propulsé par AWS (Amazon Web Services)
- accessibilité (API HTTP)
- elasticité de la volumétrie

----

<!--  data-background-image="./images/logo_Insee.png" data-background-position="center" data-background-opacity=0.2 data-background-size="cover"-->

<!-- .slide: class="slide" -->
## Coté BigData

- compatible Hadoop
- moteur SQL intégré
- chiffrement coté serveur

---

<!-- .slide: class="slide" -->
# Pilier expérience utilisateur

----

<!-- .slide: class="slide" -->
# Demo utilisateur
https://portail.innovation.insee.eu

----

Vidéo : https://levitt.fr/Onyxia.webm

----


<img src=".\images\onyxia1.png" width="70%"  height="90%" class="center">

----

<img src=".\images\onyxia2.png" width="80%"  height="90%" class="center">

----

<img src=".\images\onyxia3.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia4.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia5.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia6.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia7.png" width="80%" height="90%" class="center">

----

<img src=".\images\onyxia8.png" width="80%" height="90%" class="center">